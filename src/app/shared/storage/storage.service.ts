import { Injectable } from "@angular/core";
import {
  IStoreData,
  StoreKey,
  StoreValue
} from "../store/model/store.interface";
import { TypeGuards } from "../type-guards.class";

@Injectable({ providedIn: "root" })
export class StorageService {
  public static write(key: StoreKey, newValue: StoreValue | IStoreData): void {
    try {
      localStorage.setItem(
        key,
        TypeGuards.isPrimitive(newValue)
          ? newValue.toString()
          : JSON.stringify(newValue)
      );
    } catch (err) {
      console.error(err);
    }
  }

  public static getData(key: StoreKey): StoreValue | null {
    const res: string | null = window.localStorage.getItem(key);
    if (!res) {
      throw new Error(
        `Cant get data, Please make sure you used write(${key}) before trying to fetch it`
      );
    }
    return JSON.parse(res);
  }
}
