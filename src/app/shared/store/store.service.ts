import { Injectable } from '@angular/core';
import { BehaviorSubject, of, Observable } from 'rxjs';
import { StorageService } from '../storage/storage.service';
import { ICache, StoreValue, StoreKey } from './model/store.interface';


@Injectable({ providedIn: 'root' })
export class StoreService {
  private _cache: ICache | Object = {};

  /**
   * Set Item
   * Checks if allready cached, if so emit the new value.
   * Please Note!: the essamption here is that data is recived as a whole
   * meaning partial data will complatelly override the the previous value.
   */
  public setItem(key: StoreKey, value: StoreValue): void {
    const cached = this._cache[key];
    cached ? cached.next(value) : this._cache[key] = new BehaviorSubject(value);
    // in order to be pressitent;
    StorageService.write(key, value);
  }

  /**
   * Get Item
   * Fetch data from cache
   * If no data found, throws an error (needed to be catch and handel accordint to the spec)
   */
  public getItem<T = StoreValue>(key: StoreKey): BehaviorSubject<T> {
    if (this._cache.hasOwnProperty(key)) {
      return this._cache[key];
    } else {
      const storageData = StorageService.getData(key);
      if (storageData) {
        this.setItem(key, storageData);
        return this.getItem(key);
      }
    }
  }
}

