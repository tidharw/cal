import { BehaviorSubject } from 'rxjs';
import { IEvent } from 'src/app/features/event/model/event.interface';

export interface IStoreData {
  events: IEvent | IEvent[];
}
export type PrimitiveType = number | string | boolean;
export type StoreKey = keyof IStoreData | '__STORE__';
export type StoreValue = IStoreData[keyof IStoreData] | PrimitiveType;
export type ICache = { [key in keyof IStoreData]: BehaviorSubject<IStoreData[key]> };
