import { StoreKey } from "../model/store.interface";

export const STORE_KEY: StoreKey = "__STORE__";
