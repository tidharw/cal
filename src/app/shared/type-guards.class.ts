import { PrimitiveType } from './store/model/store.interface';

export class TypeGuards {

  static isPrimitive(input: any): input is PrimitiveType {
    return typeof input !== 'object' && typeof input !== 'function';
  }

  static isObject(input: any): input is Object {
    // null included :)
    return !TypeGuards.isPrimitive(input) && typeof input !== 'function' && !Array.isArray(input);
  }

  static isArray(input: any): input is Array<any> {
    return Array.isArray(input);
  }
}


