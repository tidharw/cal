import { Component, EventEmitter, ChangeDetectionStrategy, Input, Output } from '@angular/core';

@Component({
  selector: 'app-sides-navigation-btns',
  templateUrl: './sides-navigation-btns.component.html',
  styleUrls: ['./sides-navigation-btns.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class NavigationRightLeftBtnsComponent {
  // Didnt develop for the assignment
  public isFirst = true;
  public isLast = false;
  @Input() itemsNumber?: number;
  @Output() next: EventEmitter<void> = new EventEmitter();
  @Output() prev: EventEmitter<void> = new EventEmitter();
}
