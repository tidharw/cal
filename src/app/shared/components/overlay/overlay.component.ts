import {
  Component,
  Output,
  EventEmitter,
  ChangeDetectionStrategy
} from "@angular/core";

@Component({
  selector: "app-overlay",
  templateUrl: "./overlay.component.html",
  styleUrls: ["./overlay.component.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class OverlayComponent {
  @Output() outerClick: EventEmitter<void> = new EventEmitter();
}
