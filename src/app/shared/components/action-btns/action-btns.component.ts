import { Component, Output, EventEmitter } from "@angular/core";

@Component({
  selector: "app-action-btns",
  templateUrl: "./action-btns.component.html",
  styleUrls: ["./action-btns.component.scss"]
})
export class ActionBtnsComponent {
  @Output() delete: EventEmitter<void> = new EventEmitter();
  @Output() edit: EventEmitter<void> = new EventEmitter();
}
