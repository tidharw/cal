import { Directive, HostListener } from "@angular/core";

@Directive({
  selector: "[appNoClickBuble]"
})
export class NoBubleDirective {
  @HostListener("click", ["$event"])
  clickEvent(event: MouseEvent) {
    event.stopPropagation();
  }
}
