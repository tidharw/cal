import { NoBubleDirective } from "./directives/no-click-buble.directive";
import { OverlayComponent } from "./components/overlay/overlay.component";
import { MaterialModule } from "./material/mat.module";
import { NgModule, ModuleWithProviders } from "@angular/core";
import { StorageService } from "./storage/storage.service";
import { StoreService } from "./store/store.service";
import { ConfirmDialogComponent } from "./components/confirm-dialog/confirm-dialog.component";
import { SplitPipe } from "./pipes/split.pipe";
import { ReplacePipe } from "./pipes/replace.pipe";
import { NavigationRightLeftBtnsComponent } from "./components/sides-navigation-btns/sides-navigation-btns.component";
import { ActionBtnsComponent } from "./components/action-btns/action-btns.component";
@NgModule({
  imports: [MaterialModule],
  exports: [
    MaterialModule,
    SplitPipe,
    ReplacePipe,
    OverlayComponent,
    NoBubleDirective,
    NavigationRightLeftBtnsComponent,
    ActionBtnsComponent
  ],
  declarations: [
    ConfirmDialogComponent,
    SplitPipe,
    ReplacePipe,
    OverlayComponent,
    NoBubleDirective,
    NavigationRightLeftBtnsComponent,
    ActionBtnsComponent
  ],
  entryComponents: [ConfirmDialogComponent]
})
export class SharedModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: SharedModule,
      providers: [StoreService, StorageService]
    };
  }
}
