import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'replace'
})
export class ReplacePipe implements PipeTransform {
  transform(input: any, replace: string = '_', replacer: string = ' '): any {
    if (typeof input !== 'string') {
      return input;
    }
    const regex = new RegExp(replace, 'gm');
    return input.replace(regex, replacer);
  }

}
