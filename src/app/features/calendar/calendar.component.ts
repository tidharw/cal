import { MonthInfoMap } from './calendar-month/constants/month.constants';
import { Component, OnInit } from '@angular/core';
import { ICalendar } from './model/calendar.interface';
import { IBaseMonth } from './calendar-month/model/month.interface';

@Component({
  selector: 'app-calendar',
  templateUrl: './calendar.component.html',
  styleUrls: ['./calendar.scss']
})
export class CalendarComponent implements ICalendar, OnInit {

  public months: IBaseMonth[] = Array.from(MonthInfoMap.values());
  public currentYear: number;
  public currentDate: string;
  public currentMonth: number;

  get idPrefix(): string {
    return `${this.currentYear}_${this.months[this.currentMonth].monthName}_`;
  }

  /**
   * Get Month Days Array
   * used in the view to init the current mounth day
   * Usally I would have created a week component
   */
  get monthDaysArray(): number[] {
    const result = [];
    for (let index = 0; index < this.months[this.currentMonth].endsAt; index++) {
      result[index] = index + 1;
    }
    return result;
  }

  ngOnInit(): void {
    this.setCurrentDate();
  }

  public prevMonth() {
    this.currentMonth = this.currentMonth - 1;
  }

  public nextMonth() {
    this.currentMonth = this.currentMonth + 1;
  }

  public prevYear() {
    this.currentYear = this.currentYear - 1;
  }

  public nextYear() {
    this.currentYear = this.currentYear + 1;
  }

  private get today() {
    const date = new Date();
    return {
      currentYear: date.getFullYear(),
      currentDate: date.toLocaleDateString(),
      currentMonth: date.getMonth()
    };
  }

  /**
  * Set Current Date
  * When the user enter the Calendar the defualt view is the current month (and year)
  */
  private setCurrentDate(): void {
    const { currentYear, currentDate, currentMonth } = this.today;
    this.currentYear = currentYear;
    this.currentDate = currentDate;
    this.currentMonth = currentMonth;
  }
}

