import { IBaseMonth } from './../calendar-month/model/month.interface';

export interface ICalendar {
  months: IBaseMonth[];
}
