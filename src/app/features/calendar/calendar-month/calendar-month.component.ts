import { Component, Input, ChangeDetectionStrategy } from '@angular/core';

@Component({
  selector: 'app-calendar-month',
  templateUrl: './calendar-month.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CalendarMonthComponent {
  @Input() monthDaysNumbers: number;
  @Input() idPrefix: string;
}
