import { MonthEum } from '../model/month.enum';
import { IBaseMonth } from '../model/month.interface';

export const MonthInfoMap = new Map<MonthEum, IBaseMonth>();
MonthInfoMap.set(MonthEum.Jan, { monthName: MonthEum.Jan, monthNumber: 1, endsAt: 31 });
MonthInfoMap.set(MonthEum.Feb, { monthName: MonthEum.Feb, monthNumber: 2, endsAt: 28 });
MonthInfoMap.set(MonthEum.Mar, { monthName: MonthEum.Mar, monthNumber: 3, endsAt: 31 });
MonthInfoMap.set(MonthEum.Apr, { monthName: MonthEum.Apr, monthNumber: 4, endsAt: 30 });
MonthInfoMap.set(MonthEum.May, { monthName: MonthEum.May, monthNumber: 5, endsAt: 31 });
MonthInfoMap.set(MonthEum.Jun, { monthName: MonthEum.Jun, monthNumber: 6, endsAt: 30 });
MonthInfoMap.set(MonthEum.Jul, { monthName: MonthEum.Jul, monthNumber: 7, endsAt: 31 });
MonthInfoMap.set(MonthEum.Aug, { monthName: MonthEum.Aug, monthNumber: 8, endsAt: 31 });
MonthInfoMap.set(MonthEum.Sep, { monthName: MonthEum.Sep, monthNumber: 9, endsAt: 30 });
MonthInfoMap.set(MonthEum.Oct, { monthName: MonthEum.Oct, monthNumber: 10, endsAt: 31 });
MonthInfoMap.set(MonthEum.Nov, { monthName: MonthEum.Nov, monthNumber: 11, endsAt: 30 });
MonthInfoMap.set(MonthEum.Dec, { monthName: MonthEum.Dec, monthNumber: 12, endsAt: 31 });
