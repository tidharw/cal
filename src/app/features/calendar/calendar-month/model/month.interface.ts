import { MonthEum } from './month.enum';

export interface IBaseMonth {
  monthName: MonthEum;
  monthNumber: number;
  endsAt: number;

}

