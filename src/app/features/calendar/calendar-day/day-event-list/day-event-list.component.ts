import { IEvent } from 'src/app/features/event/model/event.interface';
import { Component, OnInit, Input, Output, EventEmitter, ChangeDetectionStrategy } from '@angular/core';

@Component({
  selector: 'app-day-event-list',
  templateUrl: './day-event-list.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class DayEventListComponent {
  @Input() eventList: IEvent[];
  @Output() openDellPopup: EventEmitter<IEvent> = new EventEmitter();
  @Output() editPopup: EventEmitter<IEvent> = new EventEmitter();

}
