import { IDay } from "./../model/day.interface";
export class DayClass implements IDay {
  public get isWeeken(): boolean {
    return this.dayNumber % 7 === 0 || this.dayNumber % 7 === 6;
  }

  constructor(public id: string, public dayNumber: number) {}
}
