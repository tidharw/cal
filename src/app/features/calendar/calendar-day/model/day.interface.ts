export interface IDay {
  id: string;
  dayNumber: number;
  isWeeken: boolean;
}
