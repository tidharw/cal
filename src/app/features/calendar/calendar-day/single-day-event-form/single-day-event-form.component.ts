import { IEvent } from 'src/app/features/event/model/event.interface';
import { Component, Output, EventEmitter, Input } from '@angular/core';
import { EventClass } from 'src/app/features/event/model/event.class';

@Component({
  selector: 'app-single-day-event-form',
  templateUrl: './single-day-event-form.component.html',
})
export class SingleDayEventFormComponent {
  @Output() addEvent: EventEmitter<IEvent> = new EventEmitter<IEvent>();
  @Output() editEvent: EventEmitter<IEvent> = new EventEmitter<IEvent>();
  @Input() id: string;
  @Input() eventData?: EventClass;

}
