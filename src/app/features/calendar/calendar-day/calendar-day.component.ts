import { IEvent } from "src/app/features/event/model/event.interface";
import { EventService } from "./../../event/event.service";
import { ChangeDetectionStrategy, Component, Input } from "@angular/core";
import { IDay } from "./model/day.interface";
import { tap } from "rxjs/operators";
import { Observable } from "rxjs";
import { EventClass } from "../../event/model/event.class";

@Component({
  selector: "app-calendar-day",
  templateUrl: "./calendar-day.component.html",
  styleUrls: ["./calendar-day.component.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CalendarDayComponent implements IDay {
  public isEditMode = false;
  public eventCounter: number;
  public eventData?: EventClass;
  public eventList$: Observable<IEvent[] | IEvent>;
  public shouldDisplayEvents = false;
  private _id: string;
  @Input() dayNumber: number;

  @Input() set id(value: string) {
    this._id = value;
    this.eventList$ = this.eventService.filterEventsByDayId(value).pipe(
      tap(events => {
        this.eventCounter = Array.isArray(events) ? events.length : 1;
      })
    );
  }
  get id(): string {
    return this._id;
  }

  get isWeeken(): boolean {
    return this.dayNumber % 7 === 0 || this.dayNumber % 7 === 6;
  }

  get isToday() {
    return this.dayNumber === new Date().getDate();
  }

  constructor(private eventService: EventService) {}

  public openDellPopup(event: IEvent): void {
    this.eventService.openDeleteEventPopup(event).subscribe(() => {
      this.closeOverlay();
    });
  }

  public editPopup($event: EventClass): void {
    this.eventData = $event;
  }

  editEvent(event: IEvent) {
    this.eventService.editEvent(event);
    this.closeOverlay();
  }
  public addEventToDay(event: IEvent): void {
    this.shouldDisplayEvents = false;
    this.eventService.addEvent(event);
    this.closeOverlay();
  }

  public closeOverlay() {
    this.shouldDisplayEvents = false;
    this.isEditMode = false;
  }
}
