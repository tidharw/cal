import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { CalendarComponent } from './calendar.component';
import { CalendarDayComponent } from './calendar-day/calendar-day.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { EventModule } from '../event/event.module';
import { CalendarMonthComponent } from './calendar-month/calendar-month.component';
import { SingleDayEventFormComponent } from './calendar-day/single-day-event-form/single-day-event-form.component';

@NgModule({
  declarations: [CalendarComponent, CalendarDayComponent, CalendarMonthComponent, SingleDayEventFormComponent],
  imports: [CommonModule, SharedModule, EventModule],
  exports: [CalendarComponent, CalendarDayComponent, SingleDayEventFormComponent]
})
export class CalendarModule { }
