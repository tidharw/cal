import { EventClass } from './../model/event.class';
import { IEvent } from 'src/app/features/event/model/event.interface';
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { HOURS_LIST } from './constants/hours-list.constant';
import { EVENT_FORM_STRUCT } from './constants/event-from-struct.constant';

@Component({
  selector: 'app-event-form-component',
  templateUrl: './event-form.component.html',
})
export class EventFormComponentComponent {
  public hoursList = HOURS_LIST;
  public isEditing = false;
  private originalEvent?: IEvent;
  public eventForm: FormGroup = this.fb.group(EVENT_FORM_STRUCT);

  @Output() eventSubmit: EventEmitter<EventClass> = new EventEmitter();
  @Output() eventEdit: EventEmitter<EventClass> = new EventEmitter();
  @Input() id: string;
  @Input() set eventData(event: IEvent) {
    if (event) {
      this.isEditing = true;
      this.eventForm.patchValue(event);
      this.originalEvent = event;
    }
  }

  constructor(private fb: FormBuilder) {
  }

  public submitEvent({ time, title, description, participates }: IEvent): void {
    if (this.eventForm.valid) {
      const submitResult: EventClass = new EventClass([this.id], time, title, description, participates);
      if (!this.isEditing) {
        this.eventSubmit.emit(submitResult);
      } else {
        submitResult.id = this.originalEvent.id;
        this.eventEdit.emit(submitResult);
      }
    }
  }
}
