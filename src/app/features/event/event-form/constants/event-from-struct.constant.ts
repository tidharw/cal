import { Validators } from '@angular/forms';

export const EVENT_FORM_STRUCT = {
  time: ['8:00', [Validators.required]],
  title: ['', [Validators.required]],
  description: [''],
  participates: ['me', [Validators.required]]
};
