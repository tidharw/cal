import { ChangeDetectionStrategy, Output, EventEmitter } from '@angular/core';
import { IEvent } from './../model/event.interface';
import { Component, Input } from '@angular/core';
import { v4 as uuid } from 'uuid';

@Component({
  selector: 'app-single-event',
  templateUrl: './single-event.component.html',
  styleUrls: ['./single-event.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SingleEventComponent implements IEvent {
  @Output() editPopup = new EventEmitter<IEvent>();
  @Output() confirmationPopup = new EventEmitter<IEvent>();
  @Input() isExpended = false;
  @Input() inDates: string[] = [];
  @Input() time: string;
  @Input() title: string;
  @Input() description: string;
  @Input() participates: string[];
  private _id: string = uuid();
  @Input() set id(value: string) {
    this._id = value;
  }
  get id(): string {
    return this._id;
  }
}
