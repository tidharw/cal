import { Component } from "@angular/core";
import { Observable } from "rxjs";
import { EventService } from "./event.service";
import { IEvent } from "./model/event.interface";

@Component({
  selector: "app-events",
  templateUrl: "./event.component.html"
})
export class EventComponent {
  public events$: Observable<IEvent[]> = this.eventService.events$;

  constructor(private eventService: EventService) {}

  /**
   * Open Delete Event Popup
   * no need to unsubscribe
   * see: openDeleteEventPopup
   */
  public openPopup(event: IEvent): void {
    this.eventService.openDeleteEventPopup(event).subscribe();
  }
}
