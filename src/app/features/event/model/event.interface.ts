export interface IEvent {
  id: string;
  inDates: string[];
  time: string;
  title: string;
  description: string;
  participates: string[];
}
