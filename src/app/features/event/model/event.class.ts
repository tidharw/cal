import { IEvent } from "./event.interface";
import { v4 as uuidv4 } from "uuid";
export class EventClass implements IEvent {
  constructor(
    public inDates: string[],
    public time: string,
    public title: string,
    public description: string,
    public participates: string[] = ["me"],
    public id: string = uuidv4()
  ) {}
}
