import { EventFormComponentComponent } from "./event-form/event-form.component";
import { SharedModule } from "./../../shared/shared.module";
import { NgModule } from "@angular/core";
import { EventComponent } from "./event.component";
import { SingleEventComponent } from "./single-event/single-event.component";
import { CommonModule } from "@angular/common";
import { ReactiveFormsModule } from "@angular/forms";
import { DayEventListComponent } from "../calendar/calendar-day/day-event-list/day-event-list.component";

@NgModule({
  imports: [CommonModule, SharedModule, ReactiveFormsModule],
  declarations: [
    EventComponent,
    SingleEventComponent,
    EventFormComponentComponent,
    DayEventListComponent
  ],
  exports: [
    EventComponent,
    SingleEventComponent,
    EventFormComponentComponent,
    DayEventListComponent
  ]
})
export class EventModule {}
