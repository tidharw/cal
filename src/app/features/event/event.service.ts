import { IEvent } from "src/app/features/event/model/event.interface";
import { ConfirmDialogComponent } from "./../../shared/components/confirm-dialog/confirm-dialog.component";
import { BehaviorSubject, Observable, of } from "rxjs";
import { StoreService } from "src/app/shared/store/store.service";
import { Injectable } from "@angular/core";
import { StoreKey } from "./../../shared/store/model/store.interface";
import { switchMap, first, tap } from "rxjs/operators";
import { MatDialog } from "@angular/material/dialog";

@Injectable({ providedIn: "root" })
export class EventService {
  public readonly FEATURE_NAME: StoreKey = "events";
  public get events$(): Observable<IEvent[]> {
    return this._events$.asObservable();
  }
  private _events$: BehaviorSubject<IEvent[]>;

  constructor(private storeService: StoreService, public dialog: MatDialog) {
    try {
      this._events$ = storeService.getItem<IEvent[]>(this.FEATURE_NAME);
    } catch (err) {
      storeService.setItem(this.FEATURE_NAME, []);
      this._events$ = storeService.getItem(this.FEATURE_NAME);
    }
  }

  /**
   * Set Events
   * see setItem docs
   * @param data
   */
  public setEvents(data: IEvent[]): void {
    this.storeService.setItem(this.FEATURE_NAME, data);
  }

  /**
   * Filter Events By Day Id
   * filters events per day
   * @param {string} dayId
   * @returns Observable<IEvent[]>
   */
  public filterEventsByDayId(dayId: string): Observable<IEvent[]> {
    const result = {};
    return this.events$.pipe(
      tap(() => (result[dayId] = [])),
      switchMap((eventItem: IEvent[]) => {
        return eventItem.map((current, index, all) => {
          if (current.inDates.find(str => str === dayId)) {
            result[dayId].push(current);
          }
          return result[dayId];
        });
      })
    );
  }

  /**
   * Open Delete Event Popup
   * will return an Observable listening to the close popup event
   * No need to un subscribe due to first() operator
   * @param {IEvent} event
   * @returns
   * @memberof EventService
   */
  public openDeleteEventPopup(event: IEvent): Observable<boolean> {
    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      height: "180px",
      width: "320px"
    });

    return dialogRef.afterClosed().pipe(
      first(),
      tap((result: boolean) => {
        if (result) {
          this.removeEvent(event.id);
        }
      })
    );
  }

  /**
   * Remove Event
   * Removes a single event by filtering it from the cuurent array abd overriding current eventList;
   * @param {string} eventId
   * @memberof EventService
   */
  public removeEvent(eventId: string): void {
    this.setEvents(
      this._events$.getValue().filter((event: IEvent) => event.id !== eventId)
    );
  }

  /**
   * Add Event
   * @param {IEvent} event
   * @memberof EventService
   */
  public addEvent(event: IEvent): void {
    const eventList = this._events$.getValue();
    eventList.push(event);
    this.setEvents(eventList);
  }

  /**
   * Edit Event
   * Seek & Replace the changed evvent
   * @param {IEvent} event
   */
  public editEvent(event: IEvent): void {
    const eventList = this._events$.getValue();
    const index = eventList.findIndex(
      (currentEvent: IEvent) => currentEvent.id === event.id
    );
    eventList[index] = event;
    this.setEvents(eventList);
  }
}
