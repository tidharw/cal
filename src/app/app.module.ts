import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
import { CalendarModule } from "./features/calendar/calendar.module";
import { AppComponent } from "./app.component";
import { SharedModule } from "./shared/shared.module";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { EventModule } from "./features/event/event.module";

@NgModule({
  declarations: [AppComponent],
  imports: [
    BrowserModule,
    CalendarModule,
    EventModule,
    BrowserAnimationsModule,
    SharedModule.forRoot()
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
